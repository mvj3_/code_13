private byte[] getSign(Context context) {
        PackageManager pm = context.getPackageManager();
        List<PackageInfo> apps = pm
                .getInstalledPackages(PackageManager.GET_SIGNATURES);
        Iterator<PackageInfo> iter = apps.iterator();

        while (iter.hasNext()) {
            PackageInfo info = iter.next();
            String packageName = info.packageName;
            //按包名 取签名
            if (packageName.equals("com.iceskysl.test")) {
                return info.signatures[0].toByteArray();

            } 
        }
        return null;
    }

public static String getPublicKey(byte[] signature) {
        try {

            CertificateFactory certFactory = CertificateFactory
                    .getInstance("X.509");
            X509Certificate cert = (X509Certificate) certFactory
                    .generateCertificate(new ByteArrayInputStream(signature));

            String publickey = cert.getPublicKey().toString();
            publickey = publickey.substring(publickey.indexOf("modulus: ") + 9,
                    publickey.indexOf("\n", publickey.indexOf("modulus:")));

            Log.d("TRACK", publickey);
            return publickey;
        } catch (CertificateException e) {
            e.printStackTrace();
        }
        return null;
    }